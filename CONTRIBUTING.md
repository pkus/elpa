How to contribute to the ELPA library:

We are very happy and gratefull if you are willing to help us improve ELPA. Thus, we would like to make this process as simple as possible for you, 
but at the same time still keep it manageable for us

For recommendations and suggestions, a simple email to us is sufficient!

If you would like to share with us your improvements, we suggest the following ways:

1. If you use a public accessible git repository, please send us a merge request. This is the preferred way
2. An email with a patch, will also be ok.

Thank you for supporting ELPA!

The ELPA development team
